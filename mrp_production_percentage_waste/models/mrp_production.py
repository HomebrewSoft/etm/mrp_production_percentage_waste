from odoo import _, api, fields, models


class MrpProduction(models.Model):
    _inherit = 'mrp.production'


    def _get_moves_raw_values(self):
        moves = super(MrpProduction, self)._get_moves_raw_values()
        if not moves:
            moves = []
        for production in self:
            for bom_line in production.bom_id.bom_line_ids:
                line_data = {
                    "qty": bom_line.operation_id.waste if bom_line.operation_id else 0,
                    "parent_line": False,
                }
                waste_move = production._get_move_raw_values(bom_line, line_data)
                moves.append(waste_move)
        return moves


